﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinnTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public void Button_Calculate_OnClick(object sender, RoutedEventArgs e)
        {
            // Get numbers as string
            string inputA = text_inputA.Text;
            string inputB = text_inputB.Text;
            string inputC = text_inputC.Text;

            // Execute calculate methode
            Calculate(inputA, inputB, inputC);
        }

        public void Calculate(string inputA, string inputB, string inputC)
        {
            int numberA;
            int numberB;
            int numberC;

            // Is "A" a valid number
            if (int.TryParse(inputA, out numberA) == false)
            {
                Message("A ist keine Zahl!");
               // ErrorHandling.Throw(typeof(FormatException), "A ist keine Zahl!");
                return;
            }

            // Is "B" a valid number
            if (int.TryParse(inputB, out numberB) == false)
            {
                Message("B ist keine Zahl!");
                //ErrorHandling.Throw(typeof(FormatException), "B ist keine Zahl!");
                return;
            }

            // Is "C" a valid number
            if (int.TryParse(inputC, out numberC) == false)
            {
                Message("C ist keine Zahl!");
               // ErrorHandling.Throw(typeof(FormatException), "C ist keine Zahl!");
                return;
            }

            // Add "A" + "B"
            int result = numberA + numberB;
            // Checks if result matches expected result
            if (result == numberC)
            {
                Message("Richtig!");
            }
            else
            {
                Message("Falsch!");
            }
        }

        private void Message(string msg)
        {
            label_status.Content = msg;
            Console.WriteLine(msg);
        }
    }
}
