<?xml version="1.0"?>
<doc>
    <assembly>
        <name>TestHelper</name>
    </assembly>
    <members>
        <member name="T:TestFramework.ErrorHandling">
            <summary>
            Class that is responsible for error handling
            </summary>
        </member>
        <member name="F:TestFramework.ErrorHandling.exception">
            <summary>
            DON'T TOUCH
            </summary>
        </member>
        <member name="F:TestFramework.ErrorHandling.output">
            <summary>
            DON'T TOUCH
            </summary>
        </member>
        <member name="M:TestFramework.ErrorHandling.Throw(System.Type,System.String,System.Int32,System.String,System.String)">
            <summary>
            Throw and save an error
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            ErrorHandling.Throw(
                 typeof(FileNotFoundException),                  // Type of exception, in this 
                                                                 // case FileNotFoundException
                 "The file " + fileName + " does not exist"      // The error message
                 )
            return;                                              // Manually abort
            \endcode
        </member>
        <member name="T:TestFramework.VersionAttribute">
            <summary>
            Defines test version
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            [Version("1.0")]
            \endcode
        </member>
        <member name="T:TestFramework.TaskAttribute">
            <summary>
            Defines the task of a test method
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            [Task("Validate correctness of method 'UpdateVersions'")]
            \endcode
        </member>
        <member name="T:TestFramework.InfoAttribute">
            <summary>
            Defines additional info
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            [Info("Checks if return value of method 'UpdateVersions' equals true")]
            \endcode
        </member>
        <member name="T:TestFramework.HeaderAttribute">
            <summary>
            Defines additional header data
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            [Header(
                 Task = "Validate correctness of method 'UpdateVersions'",
                 Author = "kfg",
                 Info = "Checks if return value of method 'UpdateVersions' equals true",
                 Description "The method 'UpdateVersions' method returns true if no error was thrown. Otherwise it will return false"
            )]
            \endcode
        </member>
        <!-- Badly formed XML comment ignored for member "T:TestFramework.TestData" -->
        <member name="T:TestFramework.TestCase">
            <summary>
            Class that is responsible for the creation of test cases
            </summary>
        </member>
        <member name="P:TestFramework.TestCase.ExpectedResult">
            <summary>
            Defines the expected result
            </summary>
        </member>
        <member name="F:TestFramework.TestCase.Parameters">
            <summary>
            Defines the test parameter
            </summary>
        </member>
        <member name="M:TestFramework.TestCase.ConvertToTestCase">
            <summary>
            Creates test case
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            new TestCase()
            {
                 ExpectedResult = true,                  // Expected result is type of "boolean"
                 Parameters = { "test1", "test2" }       // Two parameteres definied, "test1", "test2"
            }.ConvertToTestCase();
            \endcode
            \return Returns a TestCase of specified parameters and expected result
        </member>
        <member name="T:TestFramework.TestHelper">
            <summary>
            Class that is responsible for evaluating the tests
            </summary>
        </member>
        <member name="F:TestFramework.TestHelper.SHOW_POPUPS">
            <summary>
            State: Error popups will be displayed
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            #if RELEASE                                                    // Is programm in release mode => Should only
                                                                           // be the case when building the software
                 TestHelper.Init(TestHelper.SHOW_POPUPS)
            #endif
            \endcode
        </member>
        <member name="F:TestFramework.TestHelper.SHOW_DETAILLED_POPUPS">
            <summary>
            State: Error popups will be displayed with full detailed output
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            #if RELEASE                                                    // Is programm in release mode => Should only
                                                                           // be the case when building the software
                 TestHelper.Init(TestHelper.SHOW_DETAILLED_POPUPS)
            #endif
            \endcode
        </member>
        <member name="F:TestFramework.TestHelper.HIDE_POPUPS">
            <summary>
            State: Error popups are not displayed
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            #if DEBUG                                                    // Is programm in debug mode => Should only
                                                                         // be the case when testing and writing the
                                                                         // software
                 TestHelper.Init(TestHelper.HIDE_POPUPS)
            #endif
            \endcode
        </member>
        <member name="F:TestFramework.TestHelper.currentState">
            <summary>
            Current state
            </summary>
        </member>
        <member name="F:TestFramework.TestHelper.testBehaviour">
            <summary>
            Test behaviour
            </summary>
        </member>
        <member name="M:TestFramework.TestHelper.Init(System.Int32)">
            <summary>
            Initializes helper class
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            static Foo_Test()
            {
                 // Init library
            
            #if DEBUG
                 TestHelper.Init(TestHelper.HIDE_POPUPS);
            #else
                 TestHelper.Init(TestHelper.SHOW_POPUPS);
            #endif
                        
                 // Define objects, ...
                 foo = new Foo();
            }
            \endcode
        </member>
        <member name="M:TestFramework.TestHelper.SetTestBehaviour(TestBehaviour)">
            <summary>
            Sets test behaviour
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            static Foo_Test()
            {
                 TestHelper.SetTestBehaviour(TestHelper.ONLY_FAIL_ON_EXCEPTIONS);
            }
            \endcode
        </member>
        <member name="M:TestFramework.TestHelper.CheckException(System.Object)">
            <summary>
            Checks if the passed exception matches with the thrown exception
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            [TestCase[
                 typeof(DivideByZeroException),                  // Error expected
                 10,
                 0
            ])
            [TestCase[
                 null,                                           // No error expected
                 10,
                 2
            ])
            public void Division_Test(object expectedResult, int a, int b)
            {
                 foo.Division_Void(a, b);                        // foo.Calculate() does not return a value => void
                 
                 TestHelper.CheckException(expectedResult);      // Check whether an error was thrown in the method
                                                                 // "foo.Calculate_Void()" and checks if the error
                                                                 // matches the expected error
            }
            \endcode
        </member>
        <member name="M:TestFramework.TestHelper.CheckBool(System.Object,System.Boolean)">
            <summary>
            Checks if the expected bool matches with the actual return value
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            [TestCase[
                 typeof(DivideByZeroException),                      // Error expected
                 10,
                 0
            ])
            [TestCase[
                 2,                                                  // Actual value expected
                 10,
                 5
            ])
            public void Division_Test(object expectedResult, int a, int b)
            {
                 bool returnValue = foo.Division_Bool(a, b);         // foo.Calculate() returns a boolean
                 
                 returnValue.CheckBool(expectedResult);              // Check whether an error was thrown in
                                                                     // the method "foo.Calculate_Bool()".
                                                                     // If so, the error is compared with the
                                                                     // expected error.
                                                                     // If no, the return value of the method is
                                                                     // compared with the expected return value.
            }
            \endcode
        </member>
        <member name="M:TestFramework.TestHelper.CheckInt(System.Object,System.Int32)">
            <summary>
            Checks if the expected int matches with the actual return value.
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            [TestCase[
                 typeof(DivideByZeroException),                      // Error expected
                 10,
                 0
            ])
            [TestCase[
                 1,                                                  // Actual value expected (0 = false, 1 = true)
                 10,
                 5
            ])
            public void Division_Test(object expectedResult, int a, int b)
            {
                 int returnValue = foo.Division_Int(a, b);           // foo.Calculate() returns a integer
                 
                 returnValue.CheckInt(expectedResult);               // Check whether an error was thrown in the
                                                                     // method "foo.Calculate_Int()".
                                                                     // If so, the error is compared with the
                                                                     // expected error.
                                                                     // If no, the return value of the method is
                                                                     // compared with the expected return value.
            }
            \endcode
        </member>
        <member name="M:TestFramework.TestHelper.CheckString(System.Object,System.String)">
            <summary>
            Checks if the expected string matches with the actual return value.
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            [TestCase[
                 typeof(DivideByZeroException),                      // Error expected
                 10,
                 0
            ])
            [TestCase[
                 "true",                                             // Actual value expected
                 10,
                 5
            ])
            public void Division_Test(object expectedResult, int a, int b)
            {
                 string returnValue = foo.Division_String(a, b);     // foo.Calculate() returns a string
                 
                 returnValue.CheckString(expectedResult);            // Check whether an error was thrown in the
                                                                     // method "foo.Calculate_Int()".
                                                                     // If so, the error is compared with the
                                                                     // expected error.
                                                                     // If no, the return value of the method is
                                                                     // compared with the expected return value.
            }
            \endcode
        </member>
        <member name="M:TestFramework.TestHelper.CleanUp">
            <summary>
            Resets the exception
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            [SetUp]                               // Will be called before each test starts
            public void ResetExceptions()
            {
                 TestHelper.CleanUp();
            }
            \endcode
        </member>
        <member name="T:TestFramework.TestHelperExtensions">
            <summary>
            Class that is responsible for extension methods
            </summary>
        </member>
        <member name="M:TestFramework.TestHelperExtensions.CheckException(System.Object)">
            <summary>
            Checks if the passed exception matches with the thrown exception
            </summary>
        </member>
        <member name="M:TestFramework.TestHelperExtensions.CheckBool(System.Boolean,System.Object)">
            <summary>
            Checks if the expected bool matches with the actual return value.
            </summary>
        </member>
        <member name="M:TestFramework.TestHelperExtensions.CheckInt(System.Int32,System.Object)">
            <summary>
            Checks if the expected int matches with the actual return value.
            </summary>
        </member>
        <member name="M:TestFramework.TestHelperExtensions.CheckString(System.String,System.Object)">
            <summary>
            Checks if the expected string matches with the actual return value.
            </summary>
        </member>
        <member name="T:TestBehaviour">
            <summary>
            Reprents differnt behaviours for testing
            </summary>
        </member>
        <member name="F:TestBehaviour.ONLY_FAIL_ON_UNCAUGHT_EXCEPTIONS">
            <summary>
            Behaviour: The tests will only fail on uncaught exceptions
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            TestHelper.Init(TestBehaviour.ONLY_FAIL_ON_UNCAUGHT_EXCEPTIONS)
            \endcode
        </member>
        <member name="F:TestBehaviour.FAIL_ON_DIFFERENT_RESULT">
            <summary>
            Behaviour: The tests will fail on different result
            </summary>
            <b>Usage:</b>\n
            \code{.cs}
            TestHelper.Init(TestBehaviour.FAIL_ON_DIFFERENT_RESULT)
            \endcode
        </member>
    </members>
</doc>
