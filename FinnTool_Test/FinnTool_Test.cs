﻿using NUnit.Framework;
using System.Threading.Tasks;
using TestFramework;
using FinnTool;
using System;

namespace FinnTool_Test
{
    [TestFixture]
    [Apartment(System.Threading.ApartmentState.STA)]
    public class FinnTool_Test
    {
        private static MainWindow _window;

        static FinnTool_Test()
        {
            // Initialisierung des TestFrameworks
            TestHelper.Init(TestHelper.HIDE_POPUPS);
            // Setzten des Testverhaltens (Wann schlägt ein Test fehl)
            TestHelper.SetTestBehaviour(TestBehaviour.ONLY_FAIL_ON_UNCAUGHT_EXCEPTIONS);

            // Aufrufen des Konstruktors
            _window = new MainWindow();
        }

        [Test]
        [Task("Prüft die Methode 'Calculate'")]
        [TestCase(null, "2", "5", "7")]
        [TestCase(typeof(FormatException), "2", "5", "sieben")]
        public void Calculate_Test(object expectedResult, string a, string b, string c)
        {
            _window.Calculate(a, b, c);

            TestHelper.CheckException(expectedResult);
        }

        [SetUp]
        public void CleanUpException()
        {
            TestHelper.CleanUp();
        }

        #region Main Methode
        static void Main(string[] args)
        {

        }
        #endregion
    }
}